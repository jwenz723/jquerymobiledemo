// ready() function
$( document ).ready(
    // annonymous function
    function() {
        //jquery selector
        $('#messagebox').text('Dom is now ready');
    
        // display a message that states what browser the user is using
        checkBrowserType();
});

// Events with jquery selector
$(document).on('keypress', function(e) {
    var tag = e.target.tagName.toLowerCase();
    if ( e.which === 13 && tag != 'input' && tag != 'textarea' && tag != 'select' && tag != 'option') {
        event.preventDefault();
        alert('enter pressed');
    }
});


// displays an alert with which browser is viewing the page
function checkBrowserType() {
    var userAgent = navigator.userAgent;
    if((userAgent.match(/iPhone/i)) || (userAgent.match(/iPod/i)) || (userAgent.match(/iPad/i))) {
       $('#messagebox').text($('#messagebox').text() + ' - viewing from iphone/ipod');
    }
    else if (userAgent.match(/Android/i)) {
        $('#messagebox').text($('#messagebox').text() + ' - viewing from android');
    }
    else if (userAgent.match(/Chrome/i)) {
        $('#messagebox').text($('#messagebox').text() + ' - viewing from chrome');
    }
    else if (userAgent.match(/Safari/i)) {
        $('#messagebox').text($('#messagebox').text() + ' - viewing from safari');
    }
    else if (userAgent.match(/MSIE/i)) {
        $('#messagebox').text($('#messagebox').text() + ' - viewing from safari');
    }
    else if (userAgent.match(/Mozilla/i)) {
        $('#messagebox').text($('#messagebox').text() + ' - viewing from safari');
    }
}

// validate some form data using jqueryvalidate plugin
$("#complaintform").validate({
    rules: {
        name: {
            required: true
        },
        phone: {
            phoneUS: true
        }
    }
});